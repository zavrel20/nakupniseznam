package com.example.recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.random.Random

class MainActivity : AppCompatActivity(), ExampleAdapter.OnItemClickListener
{
    private val exampleList = generateList(500)
    private val adapter = ExampleAdapter(exampleList, this)
    private var selectedItem = 0;

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val recycler_view = findViewById<RecyclerView>(R.id.recycler_view)

        recycler_view.adapter = adapter
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)
    }

    private fun generateList(size: Int): ArrayList<ExampleItem>
    {
        val list = ArrayList<ExampleItem>()
        for (i in 0 until size)
        {
            val drawable = when (i % 3) {
                0 -> R.drawable.ic_baseline_fastfood
                1 -> R.drawable.ic_baseline_emoji_food_beverage_24
                else -> R.drawable.ic_baseline_food_bank_24
            }
            val item = ExampleItem(drawable,
                "Item $i",
                "Line 2")
            list += item
        }
        return list
    }

    override fun onItemClick(position: Int)
    {
        val button = findViewById<Button>(R.id.RemoveButton)
        val clickedItem = exampleList[position]
        selectedItem = position

        if (!button.isEnabled)
        {
            originalText = clickedItem.text2
            clickedItem.text2 = "Clicked"
            adapter.notifyItemChanged(position)
            button.setEnabled(true)
        }
        else
        {
            button.setEnabled(false)
            clickedItem.text2 = originalText
            adapter.notifyItemChanged(position)
        }
    }

    fun insertItem(view: View)
    {
        val item = findViewById<EditText>(R.id.product)
        val price = findViewById<EditText>(R.id.price)
        val index = Random.nextInt(8)
        val newItem = ExampleItem(
            R.drawable.ic_baseline_food_bank_24,
            item.text.toString(),
            price.text.toString()
        )
        exampleList.add(index, newItem)
        adapter.notifyItemInserted(index)
    }

    fun removeItem(view: View)
    {
        val index = selectedItem
        val button = findViewById<Button>(R.id.RemoveButton)

        exampleList.removeAt(index)
        adapter.notifyItemRemoved(index)
        button.setEnabled(false)
    }
    var originalText = ""

}